# Create your models here.
from django.db import models
from django.contrib.auth.models import User

class Admin(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        permissions = (
                       ("add_user", "can add any user or admin"),
                       ("delete", "can delete any user or admin"),
                       ("change_user", "can change any user or admin"),
                       ("block_user", "can block any user or admin"),
                       ("flag_user","can flag any user or admin"),
                       ("warn_user","can warn any user or admin"),
                       ("add_item", "can add item"),
                       ("delete_item", "can delete item "),
                       ("edit_item", "can edit item")
                      )

    def __str__(self):
        return self.user.username

class Admin_user_gp(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        permissions = (
            ("add_user", "can add any user from Member Group"),
            ("delete_user", "can delete any user from Member Group"),
            ("block_user", "can block any user from Member Group"),
        )

    def __str__(self):
        return self.user.username

class Admin_Item_gp(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        permissions = (
            ("add_user", "can add any user from Member Group"),
            ("delete_user", "can delete any user from Member Group"),
            ("block_user", "can block any user from Member Group"),
            ("add_item", "can add item"),
            ("delete_item", "can delete item "),
            ("edit_item", "can edit item")
        )

    def __str__(self):
        return self.user.username
