from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, TemplateView
from .forms import ItemModelForm
from members.models import Member
from .forms import CommentCreationForm
from .models import Item, Comment


def createComment(request):
    comment_create_form = CommentCreationForm()
    if request.method == 'POST':
        comment_create_form = CommentCreationForm(request.POST)
        if comment_create_form.is_valid():
            item = comment_create_form.cleaned_data.get('item')
            user = comment_create_form.cleaned_data.get('user')
            comment = comment_create_form.cleaned_data.get('comment')
            new_comment = Comment.objects.create(
                item=item, user=user, comment=comment
            )
            # add the user as a Member
    context = {
        'legend name ': 'Add a comment',
        'form': comment_create_form,
        'button_text': 'Add Comment'
    }
    return render(request, "item_application/item_detail.html", context)


# This class allows to view all the items posted by all the current members.
class ItemList(ListView):
    model = Item
    template_name = "item_application/items_list.html"
    paginate_by = 5

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['title'] = "All Items"
        return context


# This view allows to view the details of a selected item.
class ItemDetailView(LoginRequiredMixin, TemplateView):
    model = Item
    template_name = "item_application/item_detail.html"

    def get_context_data(self, **kwargs):
        item = get_object_or_404(Item, pk=kwargs['pk'])
        context = {'item': item}
        return context


# This view allows to create a new item.
def createItemView(request):
    item_create_form = ItemModelForm()
    if request.method == 'POST':
        item_create_form = ItemModelForm(request.POST, request.FILES)
        if item_create_form.is_valid():
            this_user = Member.objects.get(user=request.user) #fetches username of current user
            name = item_create_form.cleaned_data.get('name')
            category = item_create_form.cleaned_data.get('category')
            description = item_create_form.cleaned_data.get('description')
            keywords = item_create_form.cleaned_data.get('keywords')
            status = item_create_form.cleaned_data.get('status')
            address = item_create_form.cleaned_data.get('address')
            price = item_create_form.cleaned_data.get('price')
            itempic = item_create_form.cleaned_data.get('itempic')
            new_item = Item.objects.create(
                user=this_user, name=name, category=category,
                description=description, keywords=keywords,
                status=status, address=address, price=price, itempic=itempic
            )
            new_item.save()
            messages.success(request, 'Item {0} successfully added.'.format(name))
            return redirect('member_items_list')
    context = {
        'legend_name': 'Create New Item',
        'form': item_create_form,
        'button_text': 'Create'
    }

    return render(request, "item_application/add_item.html", context)


# This view allows to see all the items posted by the current user
class UserItemList(LoginRequiredMixin, ListView):
    model = Item
    template_name = "item_application/user_items_list.html"
    paginate_by = 5

    def get_queryset(self):
        member = Member.objects.get(user=self.request.user)
        return Item.objects.filter(user=member)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['title'] = 'My Items'
        context['h3'] = 'My Items'
        return context


# This view is to update a selected item.
def updateItemView(request, pk):
    updated_item = Item.objects.get(id=pk)
    print('The post to update', updated_item) # DELETE

    post_update_form = ItemModelForm(instance=updated_item)
    if request.method == 'POST':
        print('posted data = ', request.POST) # DELETE
        post_update_form = ItemModelForm(request.POST, request.FILES)

        if post_update_form.is_valid():
            updated_item.name = post_update_form.cleaned_data.get('name')
            updated_item.category =  post_update_form.cleaned_data.get('description')
            updated_item.description = post_update_form.cleaned_data.get('description')
            updated_item.keywords = post_update_form.cleaned_data.get('keywords')
            updated_item.status = post_update_form.cleaned_data.get('status')
            updated_item.address = post_update_form.cleaned_data.get('address')
            updated_item.price = post_update_form.cleaned_data.get('price')
            updated_item.itempic = post_update_form.cleaned_data.get('itempic')

            updated_item.save()

            messages.success(request, 'Item {0} is successfully updated'.format(updated_item.name))
            return redirect('member_items_list')

    context = {
        'legend_name': 'Update Item',
        'form': post_update_form,
        'button_text': 'Update'
    }

    return render(request, "item_application/add_item.html", context)

# This is the view to delete a selected item.
def deleteItemView(request, pk):
    item_to_delete = Item.objects.get(id=pk)
    member = Member.objects.get(user=request.user)
    # the only people who can delete are the current user,
    # superuser and admin_item_gp
    allowed_member = member.user.groups.filter(name__in=['admin_Item_gp', 'admin_gp']).exists()

    #if the current user isn't the owner of the item AND
    #the aren't a part of the admins
    if member.id != item_to_delete.user.id and allowed_member is False:
        messages.error(request, "Sorry, you are not authorized to delete this post")
        return redirect('item_list')

    if request.method == 'POST':
        item_to_delete.delete()
        messages.success(request, 'Item {0} is successfully updated'.format(item_to_delete.name))
        return redirect('member_items_list')

    context = {
        'title': 'Delete Item',
        'item_name': item_to_delete.name
    }

    return render(request, "item_application/delete_item.html", context)
