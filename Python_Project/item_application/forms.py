from django.forms import ModelForm
from .models import Item, Comment


class ItemModelForm(ModelForm):
    class Meta:
        model = Item
        fields = ['name', 'category', 'description', 'keywords', 'status', 'address', 'price', 'itempic']


class CommentCreationForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['item','user','comment']

