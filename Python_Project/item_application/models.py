from django.db import models
from members.models import Member
# Create your models here.

class Item(models.Model):
    user = models.ForeignKey(Member, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    pub_date = models.DateField(auto_now_add=True)
    keywords = models.CharField(max_length=100)
    status = models.CharField(max_length=10)
    address = models.CharField(max_length=100)
    price = models.CharField(max_length=10, default="50")
    itempic = models.ImageField(upload_to="itempics", default="default_pics/whatisit.png")

    def __str__(self):
        return self.name

class Comment(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    user = models.ForeignKey(Member, on_delete=models.CASCADE)
    comment = models.CharField(max_length=200)




