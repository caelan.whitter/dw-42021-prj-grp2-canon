# Generated by Django 3.2.3 on 2021-05-24 14:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('item_application', '0002_item_itempic'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='address',
            field=models.CharField(default='1000 Sherbrooke', max_length=100),
        ),
        migrations.AddField(
            model_name='item',
            name='status',
            field=models.CharField(default='new', max_length=10),
        ),
    ]
