from django.urls import path
from .views import *

urlpatterns = [
    path('items_list', ItemList.as_view(), name="item_list"),
    path('item_details/<int:pk>', ItemDetailView.as_view(), name="item_details"),
    path('myitemslist/', UserItemList.as_view(), name="member_items_list"),
    path('add_item/', createItemView, name="add_item"),
    path('update_item/<int:pk>', updateItemView, name="update_item"),
    path('delete_item/<int:pk>', deleteItemView, name="delete_item")
]