from django.db import models
from django.forms import ModelForm
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from item_application.models import Member


class Inbox(models.Model):
    userLogged = models.ForeignKey(Member, related_name='userLogged', on_delete=models.CASCADE)
    userTo = models.ForeignKey(Member, related_name='userTo', on_delete=models.CASCADE)

    class Meta:
        unique_together = (("userLogged", "userTo"),)


class Message(models.Model):
    sendFrom = models.ForeignKey(Member, related_name='sendFrom', max_length=100, on_delete=models.CASCADE)
    sendTo = models.ForeignKey(Member, related_name='sendTo', max_length=100, on_delete=models.CASCADE)
    message = models.TextField()
    datetime = models.DateTimeField()
    inbox = models.ForeignKey(Inbox, on_delete=models.CASCADE)



