from .models import *
from .forms import *
from django.views.generic import ListView, TemplateView, DetailView
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin


def sendMessage(request):
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            form.save()
            sendFrom = form.cleaned_data.get('sendFrom')
            sendTo = form.cleaned_data.get('sendTo')
            message = form.cleaned_data.get('message')
            datetime = form.cleaned_date.get('datetime')

            # if inbox between the two members exists (if the two users have talked before)
            if (Inbox.objects.filter(userLogged=sendFrom).filter(userTo=sendTo).count()) == 1 or \
               (Inbox.objects.filter(userLogged=sendTo).filter(userTo=sendFrom).count()) == 1:

                # create message in inbox of both users (each user gets a copy of the message)
                inbox = Inbox.objects.get(userLogged=sendFrom)
                newMessage = Message.objects.create(sendFrom=sendFrom, sendTo=sendTo, message=message, datetime=datetime, inbox=inbox)
                newMessage.save()

                inbox = Inbox.objects.get(userLogged=sendTo)
                newMessage = Message.objects.create(sendFrom=sendFrom, sendTo=sendTo, message=message, datetime=datetime, inbox=inbox)
                newMessage.save()

            # inbox does not exit (users have not messaged each other before)
            else:  # create inbox for both users and add the message
                inbox = Inbox.objects.create(userLogged=sendFrom, userTo=sendTo)
                Message.objects.create(sendFrom=sendFrom, sendTo=sendTo, message=message, datetime=datetime, inbox=inbox)

                inbox = Inbox.objects.create(userLogged=sendTo, userTo=sendFrom)
                Message.objects.create(sendFrom=sendFrom, sendTo=sendTo, message=message, datetime=datetime, inbox=inbox)

        else:
            form = MessageForm()
    return render(request, 'messageApp/privateMessage.html', {})


class InboxListView(LoginRequiredMixin, ListView):
    queryset = Inbox.objects.order_by('-id')
    template_name = 'messageApp/messages.html'
    model = Inbox

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['inbox_list'] = Inbox.objects.all()
        return context


class InboxDetailView(LoginRequiredMixin, DetailView):
    model = Inbox
    template_name = 'messageApp/privateMessage.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        inbox = get_object_or_404(Inbox, pk=kwargs['pk'])
        context = {'inbox': inbox}
        return context

