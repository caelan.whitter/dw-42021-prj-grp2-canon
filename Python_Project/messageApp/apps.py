from django.apps import AppConfig


class MessageAppConfig(AppConfig):
    name = 'messageApp'
