from django import forms
from django.forms import Textarea
from django.contrib.auth.models import User
from .models import Message


class MessageForm(forms.Form):
    sendFrom = forms.CharField(label='sendFrom', max_length=100)
    sendTo = forms.CharField(label='sendTo', max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    datetime = forms.DateTimeField()

    class Meta:
        model = Message
        fields = ['sendFrom', 'sendTo', 'message', 'datetime']