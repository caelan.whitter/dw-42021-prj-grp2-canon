from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import messages  # debug, success, error, warning, info
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import ListView
from .forms import UserRegistrationForm
from django.contrib.auth.models import Group
from .models import Member
from django.forms import ModelForm

#returns a form that allows a user to register where they automatically get registered as a Member
def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            # flash message = one time message after an event

            messages.success(request, 'Congratulations, {0}. You are now a user. Please log in.'.format(username))
            group = Group.objects.get(name='members')
            user.groups.add(group)
            # add the user as a Member
            Member.objects.create(user=user)

            return redirect('index')
    else:
        form = UserRegistrationForm()

    template_name = 'members/register.html'
    context = {'form': form, 'title': 'Register'}
    return render(request, template_name, context)

#returns home page with title
def index(request):
    context = {
        'title': 'Welcome'
    }
    return render(request, "members/index.html", context)
#the form of what to show in the settings
class CusForm(ModelForm):
    class Meta:
        model = Member
        fields = '__all__'
        exclude = ['user']

#returns an html page with the users profile picture and a link to change password
def settings(request):
    member = request.user.member
    form = CusForm(instance=member)
    if request.method == 'POST':
        form = CusForm(request.POST, request.FILES, instance = member)
        if form.is_valid():
            form.save()

    context = {
        'title': 'Settings',
        'form': form

    }

    return render(request, "members/user_settings.html", context)

#return an html page with our names on it
def creators(request):
    context = {
        'title': 'Creators',
    }
    return render(request, "members/creators.html", context)

#lists all the users with different buttons depending on what the people allowed want to do to them
class UserList(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = User
    template_name = "members/user_list.html"

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['title'] = "All Users"
        return context

    def test_func(self):
        return self.request.user.groups.filter(name__in=['admin_Item_gp', 'admin_gp', 'admin_user_gp']).exists()

    def handle_no_permission(self):
        return redirect('permission_denied')

#returns a permission denied message is user isnt allowed
def permission_denied(request):
    context = {'title': 'Access Denied'}
    messages.error(request, "Sorry you don't have authorization to view this page.")
    return render(request, "members/index.html", context)


