"""Python_Project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordChangeDoneView
from django.urls import path
from .views import *

urlpatterns = [
    path('register/', register, name='member-register'),
    path('', index, name='index'),
    path('login/', LoginView.as_view(template_name='members/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='members/logout.html'), name='logout'),
    path('settings/', settings, name='settings'),
    path('password_change/', PasswordChangeView.as_view(template_name='members/reset.html'), name='password_change'),
    path('password_change/done/', PasswordChangeDoneView.as_view(template_name='members/reset2.html'),
         name='password_change_done'),
    path('creators/', creators, name="members_creators",),
    path('userlist/', UserList.as_view(), name="user_list"),
    path('permission_denied/', permission_denied, name="permission_denied")


]
