from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

#form of what a user has to input when registering
class UserRegistrationForm(UserCreationForm):
    email: forms.CharField(max_length=180)

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
