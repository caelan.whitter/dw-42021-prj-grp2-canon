# F18 Marketplace App
Deployed at: https://f18-marketplace.herokuapp.com/

This web application allows users to register as a member to post items that they would like to sell. Visitors are restricted to view the details of an item and are requested to login as a member in order to interact with items and other members. 

Members are given the option to update, edit and delete their items and change their profile settings such as changing their password and profile picture. 
